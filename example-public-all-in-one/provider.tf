provider "aws" {
  region = var.region
}

provider "helm" {
  kubernetes {
    host                   = module.eks.cluster_endpoint
    token                  = data.aws_eks_cluster_auth.kubernetes.token
    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  }
}

provider "kubernetes" {
  host                   = module.eks.cluster_endpoint
  token                  = data.aws_eks_cluster_auth.kubernetes.token
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
}

provider "kubectl" {
  load_config_file       = false
  host                   = module.eks.cluster_endpoint
  token                  = data.aws_eks_cluster_auth.kubernetes.token
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
}