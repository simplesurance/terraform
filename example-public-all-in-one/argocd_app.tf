
resource "kubectl_manifest" "argocd_app" {
  yaml_body  = file("${path.module}/argocd_app.yaml")
  depends_on = [module.helm_release]
}