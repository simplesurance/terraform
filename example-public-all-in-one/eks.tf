module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "20.11.1"

  create = var.enabled

  cluster_name                   = var.cluster_name
  cluster_version                = var.cluster_version
  cluster_endpoint_public_access = true

  cluster_ip_family          = "ipv4"
  create_cni_ipv6_iam_policy = false

  enable_cluster_creator_admin_permissions = true

  enable_efa_support = true

  cluster_addons = var.cluster_addons

  vpc_id                   = module.vpc.vpc_id
  subnet_ids               = module.vpc.private_subnets
  control_plane_subnet_ids = module.vpc.intra_subnets

  eks_managed_node_group_defaults = {
    ami_type       = "AL2_x86_64"
    instance_types = ["m6i.large", "m5.large", "m5n.large", "m5zn.large"]
  }

  eks_managed_node_groups = var.managed_node_groups

  access_entries = {
    example = {
      kubernetes_groups = []
      principal_arn     = aws_iam_role.this.arn

      policy_associations = {
        edit = {
          policy_arn = "arn:aws:eks::aws:cluster-access-policy/AmazonEKSEditPolicy"
          access_scope = {
            namespaces = ["default"]
            type       = "namespace"
          }
        }
        view = {
          policy_arn = "arn:aws:eks::aws:cluster-access-policy/AmazonEKSViewPolicy"
          access_scope = {
            type = "cluster"
          }
        }
      }
    }
  }

  tags = merge(local.tags, var.additional_tags)
}
