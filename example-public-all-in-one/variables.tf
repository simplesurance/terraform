variable "cluster_name" {
  type = string
}

variable "cluster_version" {
  type = string
}

variable "region" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "vpc_name" {
  type = string
}

variable "enabled" {
  default = true
  type    = bool
}

variable "managed_node_groups" {
  description = "Configuration for EKS managed node groups"
  type = map(object({
    ami_type                   = string
    name                       = string
    ami_id                     = string
    min_size                   = number
    max_size                   = number
    desired_size               = number
    enable_bootstrap_user_data = bool
    capacity_type              = string
    labels                     = map(string)
    taints = list(object({
      key    = string
      value  = string
      effect = string
    }))
    block_device_mappings = map(object({
      device_name = string
      ebs = object({
        volume_size           = number
        volume_type           = string
        iops                  = number
        throughput            = number
        encrypted             = bool
        delete_on_termination = bool
      })
    }))
    instance_types = list(string)
  }))
  default = {}
}

variable "cluster_addons" {
  description = "Configuration for EKS cluster addons"
  type = map(object({
    addon_name                  = optional(string)
    addon_version               = optional(string)
    resolve_conflicts_on_create = optional(string)
    resolve_conflicts_on_update = optional(string)
    tags                        = optional(map(string))
    preserve                    = optional(bool)
    service_account_role_arn    = optional(string)
    configuration_values        = optional(string)
    most_recent                 = optional(bool)
    before_compute              = optional(bool)
  }))
  default = {}
}

variable "additional_tags" {
  type = map(string)
}

variable "argocd_gitlab_repo_password" {
  sensitive = true
  type      = string
}