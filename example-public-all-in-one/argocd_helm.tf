module "helm_release" {
  source = "cloudposse/helm-release/aws"
  # Cloud Posse recommends pinning every module to a specific version
  version = "v0.10.1"

  name = "argocd"

  repository    = "https://argoproj.github.io/argo-helm"
  chart         = "argo-cd"
  chart_version = "6.11.1"

  create_namespace     = true
  kubernetes_namespace = "argocd"

  atomic          = true
  cleanup_on_fail = true
  timeout         = 300
  wait            = true

  # These values will be deep merged
  values = [file("${path.module}/argocd_values.yaml")]

  set_sensitive = [{
    name  = "configs.repositories.gitlab.password"
    value = var.argocd_gitlab_repo_password
    type  = "auto"
  }]

  # Enable the IAM role
  iam_role_enabled = false

  # Add the IAM role using set()
  service_account_role_arn_annotation_enabled = false

  eks_cluster_oidc_issuer_url = module.eks.oidc_provider

  depends_on = [
    module.eks
  ]
}