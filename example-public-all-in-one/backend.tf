terraform {
  backend "s3" {
    bucket = "aokhotnikov-tf-state"
    key    = "example-public-all-in-one/terraform.tfstate"
    region = "eu-central-1"
    # dynamodb_table = "aokhotnikov-tflock" # lack of permissions
  }
}
