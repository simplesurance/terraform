cluster_version = "1.30"
cluster_name    = "aokhotnikov-example-1"
cluster_addons = {
  coredns = {
    most_recent    = true
    before_compute = false
  }
  kube-proxy = {
    most_recent    = true
    before_compute = false
  }
  vpc-cni = {
    most_recent    = true
    before_compute = true
  }
}
region   = "eu-central-1"
vpc_cidr = "10.1.0.0/16"
vpc_name = "aokhotnikov-example-1"
# aws ssm get-parameter --name /aws/service/eks/optimized-ami/1.29/amazon-linux-2/recommended/image_id --region eu-central-1 --query "Parameter.Value" --output text
# aws ssm get-parameter --name /aws/service/eks/optimized-ami/1.29/amazon-linux-2-arm64/recommended/image_id --region eu-central-1 --query "Parameter.Value" --output text
managed_node_groups = {
  custom_ami = {
    ami_type                   = "AL2_ARM_64"
    name                       = "default-arm"
    ami_id                     = "ami-06e7f7c58f561472d"
    min_size                   = 3
    max_size                   = 3
    desired_size               = 3
    enable_bootstrap_user_data = true
    capacity_type              = "SPOT"
    labels = {
      managed-by = "terraform"
    }
    taints = []
    block_device_mappings = {
      xvda = {
        device_name = "/dev/xvda"
        ebs = {
          volume_size           = 30
          volume_type           = "gp3"
          iops                  = 3000
          throughput            = 150
          encrypted             = false
          delete_on_termination = true
        }
      }
    }
    instance_types = ["t4g.medium"]
  }
}
additional_tags = {
  env   = "testing"
  owner = "aokhotnikov"
  team  = "sre"
}
